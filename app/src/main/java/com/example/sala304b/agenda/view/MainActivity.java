package com.example.sala304b.agenda.view;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.sala304b.agenda.R;
import com.example.sala304b.agenda.model.Agenda;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listViewAgendas;
    private ArrayAdapter<Agenda>adapter;
    private Agenda contatoSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewAgendas = findViewById(R.id.listViewAgendas);

        Agenda contato1 = new Agenda("Joao da Silva","Rua Sete, n°50,Vila Velha - ES", "2222 2222","99999 9999","foto");
        Agenda contato2 = new Agenda("Franciso Santos","Rua oito, n°60,Vitoria - ES", "3333 3333","99999 9999","foto");
        Agenda contato3 = new Agenda("Maria da Silva","Rua nove, n°70,Serra - ES","4444 4444","99999 9999","foto");

        List<Agenda> lista = new ArrayList<>();

        lista.add(contato1);
        lista.add(contato2);
        lista.add(contato3);

        adapter = new ArrayAdapter<Agenda>(this,android.R.layout.simple_list_item_1,lista);

        listViewAgendas.setAdapter(adapter);

        listViewAgendas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posicao, long indice) {

                contatoSelecionado = (Agenda)adapter.getItem(posicao);


                return false;
            }
        });

        registerForContextMenu(listViewAgendas);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        MenuItem menuLigarTelefone = menu.add("Ligar telefone");

        menuLigarTelefone.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent intentDeLigar = new Intent(Intent.ACTION_CALL);
                Uri discar = Uri.parse("telefone:" + contatoSelecionado.getTelefone());
                intentDeLigar.setData(discar);
                startActivity(intentDeLigar);
                return false;
            }

        });



        MenuItem menuLigarCelular = menu.add("Ligar Celular");

        menuLigarCelular.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent intentDeLigar = new Intent(Intent.ACTION_CALL);
                Uri discar = Uri.parse("celular:" + contatoSelecionado.getCelular());
                intentDeLigar.setData(discar);
                startActivity(intentDeLigar);


                return false;
            }
        });

        }



    }
}
